# xivo-usage-collector

It's an agent that collects usage data from USM database. The usage data are written in the database by xivo-usage-writer.  
The agent aggregates usage data into specific metrics, which are then daily sent to the USM-backend.

## Debbugging

It has proven hard to debug a running telegraf process. However its config can be tested in parallel as shown below.  
`xivo-dcomp up -d usage_collector` restarts the telegraf only if the image/env values have changed. If you just change the .conf files, restart the container with `xivo-dcomp restart usage_collector`.

### test telegraf config

within the container
```bash
telegraf --config /etc/telegraf/telegraf.conf --config-directory /etc/telegraf/telegraf.d --test
```
+ Does run the inputs, processors and aggregators and display the line protocol that would be sent to outputs
+ Does not test the ouputs
```bash
telegraf --config /etc/telegraf/telegraf.conf --config-directory /etc/telegraf/telegraf.d --once
```
+ Does run the whole telegraf process from inputs to outputs once
+ Does not displays the line protocol it sends them

### agent level logging

provide those two entries to the telegraf agent config so the logs are more verbose
```toml
[agent]
  debug = true
  quiet = false
```

### add internal logs

Add the internal input `[[inputs.internal]]` to inputs.conf so the errors may be followed. See plugin readme [there](https://github.com/influxdata/telegraf/blob/master/plugins/inputs/internal/README.md)

Sample configuration
+ inputs.conf:
```toml
[[inputs.internal]]
```
+ outputs.conf
```toml
[[outputs.file]]
  ## Files to write to, "stdout" is a specially handled file.
  files = ["stdout", "/home/telegraf/collector-logs.out"]
  namepass = ["internal_*"]
  rotation_max_size = "200MB"
  rotation_max_archives = 5
```
