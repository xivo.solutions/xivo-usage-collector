#!/usr/bin/env bash
set -e

if [ -z "$TARGET_VERSION" ]; then
    echo "TARGET_VERSION is not available"
    exit 1
fi

sed -i "s/##TARGET_VERSION##/$TARGET_VERSION/" docker/telegraf/telegraf.d/inputs.conf

docker build --no-cache --build-arg TARGET_VERSION="$TARGET_VERSION" -t xivoxc/xivo-usage-collector:"$TARGET_VERSION" ./docker/
