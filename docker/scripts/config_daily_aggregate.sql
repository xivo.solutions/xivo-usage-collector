SELECT
    date_trunc('day', "time") AS metric_time,
    edge_activated,
    switchboard_activated,
    visio_activated,
    users_group_across_mds_activated,
    xivolts,
    xivoversion,
    mobile_app_activated,
    MAX(nb_mapp_android_users)::INTEGER as nb_mapp_android_users,
    MAX(nb_mapp_ios_users)::INTEGER as nb_mapp_ios_users,
    MAX(nb_agents)::INTEGER as nb_agents,
    MAX(nb_mds)::INTEGER as nb_mds,
    MAX(nb_meetingrooms)::INTEGER as nb_meetingrooms,
    MAX(nb_switchboards)::INTEGER as nb_switchboards,
    MAX(nb_users)::INTEGER as nb_users,
    MAX(nb_webrtc_users)::INTEGER as nb_webrtc_users,
    MAX(ldap_user_sync)::INTEGER as ldap_user_sync
FROM cfg.config_xivo
WHERE
    "time" >= date_trunc('day', timezone('utc', now())) - INTERVAL '1 DAY' and "time" < date_trunc('day', timezone('utc', now()))
GROUP BY
    metric_time,
    edge_activated,
    switchboard_activated,
    visio_activated,
    xivolts,
    xivoversion,
    mobile_app_activated,
    users_group_across_mds_activated
ORDER BY metric_time desc;
