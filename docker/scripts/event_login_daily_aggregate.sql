SELECT
    date_trunc('day', "time") AS metric_time,
    COUNT(DISTINCT "userId") AS total_distinct_user_logins,
    SUM(CASE WHEN "softwareType" = 'webBrowser' THEN 1 ELSE 0 END) AS sw_type_web_browser_logins,
    SUM(CASE WHEN "softwareType" = 'electron' THEN 1 ELSE 0 END) AS sw_type_electron_logins,
    SUM(CASE WHEN "softwareType" = 'mobile' THEN 1 ELSE 0 END) AS sw_type_mobile_logins,
    SUM(CASE WHEN "softwareType" = 'unknown' THEN 1 ELSE 0 END) AS sw_type_unknown_logins,
    SUM(CASE WHEN "lineType" = 'phone' THEN 1 ELSE 0 END) AS ln_type_phone_logins,
    SUM(CASE WHEN "lineType" = 'webRTC' THEN 1 ELSE 0 END) AS ln_type_webrtc_logins,
    SUM(CASE WHEN "lineType" = 'ua' THEN 1 ELSE 0 END) AS ln_type_ua_logins,
    SUM(CASE WHEN "applicationType" = 'uc' THEN 1 ELSE 0 END) AS app_type_uc_logins,
    SUM(CASE WHEN "applicationType" = 'ccagent' THEN 1 ELSE 0 END) AS app_type_ccagent_logins,
    SUM(CASE WHEN "applicationType" = 'ccmanager' THEN 1 ELSE 0 END) AS app_type_ccmanager_logins,
    SUM(CASE WHEN "applicationType" = 'switchboard' THEN 1 ELSE 0 END) AS app_type_switchboard_logins,
    SUM(CASE WHEN "applicationType" = 'unknown' THEN 1 ELSE 0 END) AS app_type_unknown_logins,
    SUM(CASE WHEN "applicationType" = 'undefined' THEN 1 ELSE 0 END) AS app_type_undefined_logins
FROM evt.event_login
WHERE
    "time" >= date_trunc('day', timezone('utc', now())) - INTERVAL '1 DAY' and "time" < date_trunc('day', timezone('utc', now()))
GROUP BY metric_time
LIMIT 1;
